import React, {Component} from 'react'
import axios from "axios/index";
import * as FileSaver from "file-saver";
import AWS from 'aws-sdk';
import TopBar from "../../components/TopBar/TopBar";
import ActionsBar from "../../components/ActionsBar/ActionsBar";
import Files from "../../components/Files/Files";
import Spinner from "../../components/Spinner/Spinner";

import {
BUCKET,
DELETE_FILES,
DOWNLOAD_FILES,
GRAYSCALE_FILES,
LIST_BUCKETS,
ROTATE_FILES,
UPLOAD_FILE
} from '../../resources/ApiConstants'

const BUCKET_REGION = 'eu-west-2';
const IDENTITY_POOL_ID = 'eu-west-2:f6aed0f2-010b-4637-8ad0-d58b043e407f';

class Bucket extends Component {

  constructor(props) {
    super(props);
    this.state = {
      buckets: [],
      selectedBucket: null,

      files: [],
      filteredFiles: [],
      selectedFiles: [],

      filterInputValue: '',
      allSelected: false,
      loadingFiles: false,
      loadingBuckets: true
    };

    AWS.config.update({
      region: BUCKET_REGION,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IDENTITY_POOL_ID
      })
    });
  }

  componentDidMount() {
    let buckets = [];
    axios.get(LIST_BUCKETS).then(response => {
      response.data.forEach(bucketName => {
        buckets.push({
          name: bucketName
        })
      });

      this.setState({
        buckets: buckets,
        loadingBuckets: false
      })
    });
  }

  filterFiles = (event) => {
    const input = event.target.value;

    let newFilteredFiles = this.state.files.filter(file => {
      return file.name.includes(input)
    });

    this.setState({
      filterInputValue: input,
      filteredFiles: newFilteredFiles
    })
  };

  selectBucketHandler = (selectedBucket) => {
    this.setState({
      loadingFiles: true
    });
    let filesFromTheServer = [];
    axios.get(BUCKET(selectedBucket.name)).then(response => {
      response.data.files.forEach(file => {
        filesFromTheServer.push({
          name: file.name
        })
      });

      this.s3 = new AWS.S3({
        apiVersion: '2006-03-01',
        params: {Bucket: selectedBucket.name}
      });

      this.setState({
        files: filesFromTheServer,
        filteredFiles: filesFromTheServer,
        selectedBucket: selectedBucket,
        loadingFiles: false
      })
    });
  };

  clearFilterInputHandler = () => {
    this.setState({
      filterInputValue: '',
      filteredFiles: this.state.files
    })
  };

  selectFileHandler = (selectedFile) => {
    let newSelected = [...this.state.selectedFiles];

    if (newSelected.includes(selectedFile)) {
      const indexInSelected = newSelected.indexOf(selectedFile);
      newSelected.splice(indexInSelected, 1);
    } else {
      newSelected.push(selectedFile);
    }

    this.setState({
      selectedFiles: newSelected
    })
  };

  uploadNewFilesHandler = (event) => {
    const url = UPLOAD_FILE(this.state.selectedBucket.name);
    const files = [...event.target.files];

    files.forEach(file => {
      const data = new FormData();
      data.append("file", file.name);
      return axios.post(url, data).then(response => {
        if (response.status === 200) {
          this.uploadDirectlyToS3(response.data, file);
          this.setState(prevState => ({
            files: [...prevState.files, {name: file.name}],
            filteredFiles: [...prevState.filteredFiles, {name: file.name}]
          }))
        } else {
          this.logResponseInfo(response);
        }
      })
    });
  };

  uploadDirectlyToS3 = (preSignedUrl, file) => {
    return axios.put(preSignedUrl, file).then(response => {
      this.logResponseInfo(response);
    })
  };

  deleteSelectedHandler = () => {
    this.setState({
      loadingFiles: true
    });
    const url = DELETE_FILES(this.state.selectedBucket.name);
    let data = new FormData();
    data.append('files', this.state.selectedFiles.map(file => file.name));

    return axios.post(url, data).then(response => {
      if (response.status === 200) {
        const files = [...this.state.files];
        const selectedFiles = [...this.state.selectedFiles];
        let newFiles = files.filter(file => {
          return !selectedFiles.includes(file);
        });

        this.setState({
          files: newFiles,
          filteredFiles: newFiles,
          selectedFiles: [],
          filterInputValue: '',
          loadingFiles: false
        });
      } else {
        this.logResponseInfo(response);
        this.setState({
          loadingFiles: false
        });
      }
    });
  };

  rotateSelectedHandler = () => {
    const url = ROTATE_FILES(this.state.selectedBucket.name);
    let data = new FormData();
    data.append('files', this.state.selectedFiles.map(file => file.name));

    return axios.post(url, data).then(response => this.logResponseInfo(response));
  };

  grayscaleSelectedHandler = () => {
    const url = GRAYSCALE_FILES(this.state.selectedBucket.name);
    let data = new FormData();
    data.append('files', this.state.selectedFiles.map(file => file.name));

    return axios.post(url, data).then(response => this.logResponseInfo(response));
  };

  downloadSelectedHandler = () => {
    const url = DOWNLOAD_FILES(this.state.selectedBucket.name);
    let data = new FormData();
    data.append('files', this.state.selectedFiles.map(file => file.name));

    return axios.post(url, data).then(response => {
      if (response.status === 200) {
        response.data.forEach(preSignedUrl => {
          FileSaver.saveAs(preSignedUrl, preSignedUrl);
        });
      } else {
        this.logResponseInfo(response)
      }
    });
  };

  selectAllHandler = () => {
    if (this.state.allSelected) {
      this.setState({
        selectedFiles: [],
        allSelected: false
      })
    } else {
      const newSelectedFiles = [...this.state.files];
      this.setState({
        selectedFiles: newSelectedFiles,
        allSelected: true
      })
    }
  };

  logResponseInfo = (response) => {
    console.log(response.status + ' - ' + response.data);
  };

  render() {
    let topBar = <TopBar buckets={this.state.buckets}
                         selectedBucket={this.state.selectedBucket}
                         filterInputValue={this.state.filterInputValue}
                         selectAllHandler={this.selectAllHandler}
                         onSelectBucketChange={this.selectBucketHandler}
                         onFilterInputChange={this.filterFiles}
                         onFilterInputRemoveButtonClick={this.clearFilterInputHandler}/>;
    if (this.state.loadingBuckets) {
      topBar = <Spinner/>
    }

    return (
        <div>
          {topBar}
          <Files files={this.state.filteredFiles}
                 selectedFiles={this.state.selectedFiles}
                 loadingFiles={this.state.loadingFiles}
                 selectedBucket={this.state.selectedBucket}
                 onClick={this.selectFileHandler}
          />
          {this.state.selectedBucket ? <ActionsBar isSomethingSelected={this.state.selectedFiles.length > 0}
                                                   onUploadButtonClick={this.uploadNewFilesHandler}
                                                   onDeleteButtonClick={this.deleteSelectedHandler}
                                                   onDownloadButtonClick={this.downloadSelectedHandler}
                                                   onRotateButtonClick={this.rotateSelectedHandler}
                                                   onGrayscaleButtonClick={this.grayscaleSelectedHandler}
          /> : null}
        </div>
    )
  }
}

export default Bucket