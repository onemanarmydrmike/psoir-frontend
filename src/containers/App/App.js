import React, {Component} from 'react';
import classes from './App.css';
import Header from "../../components/Header/Header";
import Footer from "../../components/Footer/Footer";
import Bucket from "../Bucket/Bucket";

export default class App extends Component {
  render() {
    return (
        <div className={classes.App}>
          <Header/>
          <Bucket/>
          <Footer/>
        </div>
    );
  }
}