import React from 'react'
import classes from './ActionsBar.css'
import {Button} from 'react-bootstrap'
import FileInputButton from "../BootstrapLike/FileInputButton/FileInputButton";

const actionsBar = (props) => {
  return (
      <div className={classes.ActionsBar}>
        <FileInputButton title='Upload new files'
                         onChangeHandler={props.onUploadButtonClick}/>
        <Button className={classes.Button}
                bsStyle="danger"
                disabled={!props.isSomethingSelected}
                onClick={props.onDeleteButtonClick}>Delete selected</Button>
        <Button className={classes.Button}
                bsStyle="info"
                disabled={!props.isSomethingSelected}
                onClick={props.onDownloadButtonClick}>Download selected</Button>
        <br/>
        <Button className={classes.Button}
                bsStyle="warning"
                disabled={!props.isSomethingSelected}
                onClick={props.onRotateButtonClick}>Rotate selected</Button>
        <Button className={classes.Button}
                bsStyle="warning"
                disabled={!props.isSomethingSelected}
                onClick={props.onGrayscaleButtonClick}>Grayscale selected</Button>
      </div>
  )
};

export default actionsBar