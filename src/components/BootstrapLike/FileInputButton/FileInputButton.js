import React from 'react'
import classes from '../../ActionsBar/ActionsBar.css'

const fileInputButton = (props) => {
  const fileInputButtonClasses = [classes.Button, "btn", "btn-success", "btn-file"];

  return (
      <label className={fileInputButtonClasses.join(' ')}>
        {props.title}
        <input type="file" multiple={true} style={{display: "none"}}
               onChange={props.onChangeHandler}/>
      </label>
  )
};

export default fileInputButton