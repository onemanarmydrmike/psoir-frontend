import React from 'react'
import classes from './Header.css'

const header = () => {
  return (
      <div className={classes.Header}>
        <h1>Hello World!</h1>
        <h2>This is PSOiR Lab project created for university purpose</h2>
      </div>
  )
};

export default header;
