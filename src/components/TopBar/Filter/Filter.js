import React from 'react'
import {Button, Form, FormControl, Glyphicon} from "react-bootstrap";

const filter = (props) => {
  return (
      <Form inline style={{float: 'right'}}>
        <Glyphicon glyph="search"/>{' '}
        <FormControl type="text"
                     placeholder="Search..."
                     value={props.filterInputValue}
                     onChange={props.onFilterInputChange}/>
        <Button bsStyle='danger' onClick={props.onFilterInputRemoveButtonClick}>
          <Glyphicon glyph='remove'/>
        </Button>
      </Form>
  )
};

export default filter