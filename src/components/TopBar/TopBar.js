import React from 'react'
import classes from './TopBar.css'
import Filter from "./Filter/Filter";
import {Button, DropdownButton, MenuItem} from "react-bootstrap";

const topBar = (props) => {

  const selectBucketTitle = props.selectedBucket ? props.selectedBucket.name : 'select bucket';

  return (
      <div className={classes.TopBar} style={{position: 'relative'}}>
        {props.selectedBucket ?
            <Button bsStyle='default'
                    style={{float: 'left'}}
                    onClick={props.selectAllHandler}>All
            </Button> : null}
        <DropdownButton /*style={{float: 'left'}}*/
            bsStyle='primary'
            title={'Bucket: ' + selectBucketTitle}
            id='selectBucket'
            onSelect={props.onSelectBucketChange}>
          {props.buckets.map(bucket => {
            return <MenuItem key={bucket.name} eventKey={bucket}>{bucket.name}</MenuItem>
          })}
        </DropdownButton>
        {props.selectedBucket ? <Filter {...props}/> : null}
      </div>
  )
};

export default topBar