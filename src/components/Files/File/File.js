import React from 'react'

const file = (props) => {
  const {file} = props;

  const style = {
    display: 'block',
    cursor: 'default'
  };

  return (
      <div style={style} onClick={() => props.onClick(file)}>
        <input id={file.name} type='checkbox' checked={props.checked} readOnly={true}/>
        {' ' + file.name}
      </div>
  )
};

export default file