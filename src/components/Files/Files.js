import React from 'react'
import classes from './Files.css'
import File from "./File/File";
import Scrollbar from 'react-custom-scrollbars';
import Spinner from "../Spinner/Spinner";

const files = (props) => {
  let content = props.files.map((file) => {
    return <File key={file.name}
                 checked={props.selectedFiles.some(f => f.name === file.name)}
                 file={file}
                 onClick={props.onClick}/>
  });

  if (props.loadingFiles) {
    content = <Spinner/>
  } else if (props.files.length === 0 && props.selectedBucket) {
    content = <p className={classes.P}>there is no picture yet<br/>upload some first</p>
  }

  return (
      <Scrollbar style={{height: 300}} className={classes.Files}>
        {content}
      </Scrollbar>
  )
};

export default files